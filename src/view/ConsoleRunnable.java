package view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import db.Services;
import model.Console;

public class ConsoleRunnable implements Runnable {

	private ServerSocket serverSocket;
	private Socket socket;

	public ConsoleRunnable(ServerSocket serverSocket) {
		this.serverSocket = serverSocket;
	}

	@Override
	public void run() {
		try {
			while(true){
				socket = serverSocket.accept();
				connect();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private void connect(){
		try {
			InputStream is = socket.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);

			BufferedReader br = new BufferedReader(isr);

			String consoleId = br.readLine();

			System.out.println("---La console " + consoleId + " est connect�e---");

			Console console = Services.getConsoleById(consoleId);
			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			out.writeObject(console);
			out.flush();
			out.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
