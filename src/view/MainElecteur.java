package view;

import java.io.BufferedReader; 
import java.io.File; 
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

import model.Console;
import model.Candidat; 
import model.Electeur;
import services.ServicesVoteInterface;

public class MainElecteur {

	static Scanner clavier = new Scanner(System.in);
	static Electeur electeur = null;
	static ServicesVoteInterface serviceVote;
	static Console console;

	public static void main(String args[]) throws Exception{
		setConsole();
		console.startClient();
		serviceVote = console.getServices();

		if(!serviceVote.getCurrentElectionId().isEmpty()){
			login();	
		}else{
			System.out.println("Aucune election est en cours!");
		} 
	}

	private static void login() throws Exception {
		boolean canTry = true;
		while(canTry){

			System.out.print("Nom : ");
			String nom = clavier.nextLine();

			System.out.print("Numero d'identification : ");
			String num = clavier.nextLine();

			electeur = serviceVote.login(nom, num, console.getId());
			if(electeur!=null){
				canTry = false;
				showMenu(); 
			}else{
				System.out.println("Electeur n'existe pas");
			}
			System.out.println();
		}
	}

	private static void showMenu() throws Exception {
		System.out.println(); 

		System.out.println("Bienvenue " + electeur.getFullName());
		System.out.println("==================================="); 

		if(!electeur.hasVoted()){	 
			System.out.print("Vous voulez voter ? (Y/N) ? : "); 
			String input = clavier.nextLine();
			switch(input.toLowerCase()){
			case "y":
				vote();
			case "n":
				logOut();
			default:
				logOut();
			}
		}else{
			System.out.println("Votre vote a bien �t� pris en compte."); 
			logOut();
		} 
	}

	private static void logOut() throws Exception{
		System.out.println();
		System.out.println("Merci!");
		System.out.println("//////////////////////////////////////////////////////");
		System.out.println();
		electeur = null;
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) { 
			e.printStackTrace();
		}

		login();
	}

	private static void vote() throws Exception{
		System.out.println();
		System.out.println("Candidats ( Vous devez ecrire le numero du candidat) : ");
		System.out.println("-----------------------------------------");
		List<Candidat> candidats = serviceVote.getListCandidat();

		for (Candidat candidat : candidats){
			System.out.println(candidat.getName());
		}
		System.out.println();
		System.out.print("Vous votez pour : " );
		String candidatVoteId = clavier.nextLine();

		boolean goodVoting = false;
		for(Candidat candidat : candidats){
			if(candidatVoteId.equals(candidat.getId())){
				goodVoting = true;
				break;
			}
		}

		if(goodVoting){ 
			serviceVote.addVote(electeur.getId(), candidatVoteId);
			System.out.println("Votre vote a bien �t� pris en compte.");
			logOut();
		}else{
			System.out.println("Votre vote n'a pas �t� pris en compte. Vous devez ecrire le numero du candidat. Deconnexion en cours!");
			logOut();
		}

	}

	private static void setConsole(){ 
		BufferedReader reader=null;
		String consoleId = "";
		int serveurPort = 0;
		String serveurHote = "";

		try{
			reader = new BufferedReader(new FileReader(new File("console_id.txt")));
			String line = reader.readLine();
			consoleId = line.replace("console_id=", "");
			line = reader.readLine();
			serveurHote =  line.replace("serveur_hote=", "");
			line = reader.readLine();
			serveurPort = Integer.parseInt(line.replace("serveur_port=", ""));
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				reader.close();
			} catch (IOException e) { 
				e.printStackTrace();
			}
		}

		console = getConsoleFromServer(consoleId, serveurHote, serveurPort);

	} 

	private static Console getConsoleFromServer(String consoleId, String serveurHote, int serveurPort){

		Socket socket = null;
		PrintWriter pw = null;
		try {
			socket = new Socket(serveurHote,serveurPort+1);

			OutputStream os = socket.getOutputStream();
			pw = new PrintWriter(os);
			pw.println(consoleId);
			pw.flush();

			InputStream is = socket.getInputStream();
			ObjectInputStream ois = new ObjectInputStream(is);

			Console console = (Console) ois.readObject();
			return console;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}finally{
			try {
				socket.close();
				pw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}