package view;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

import model.Message;
import model.Serveur;

public class ProblemeElectionRunnable implements Runnable {

	private Serveur serveur;
	private ServerSocket serverSocket;
	private Socket socket;

	public ProblemeElectionRunnable(Serveur serveur, ServerSocket serverSocket) {
		this.serveur = serveur;
		this.serverSocket = serverSocket;
	}

	@Override
	public void run() {
		try {
			while(true){
				socket = serverSocket.accept();
				connect();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void connect(){
		try {
			InputStream is = socket.getInputStream();
			ObjectInputStream ois = new ObjectInputStream(is);
			Message message = (Message) ois.readObject();

			if (message.getType() == Message.REQUETE) {
				serveur.surReceptionDeRequete(message);
			}else{
				serveur.surReceptionDeConfirmation(message);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
