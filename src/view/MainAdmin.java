package view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import services.ServicesAdministrateur;
import model.Serveur;

public class MainAdmin {

	static Scanner clavier = new Scanner(System.in);
	static Serveur serveur;

	public static void main(String[] args) {
		setServeur();
		serveur.startServer(); 

		System.out.print("Vous voulez arreter l'election (Y/N) ? : "); 
		String input = clavier.nextLine();
		if(input.toLowerCase().equals("y")){
			stopElection();
		}

	}

	private static void setServeur(){
		BufferedReader reader=null; 
		int serveurId = 0; 

		try{
			reader = new BufferedReader(new FileReader(new File("serveur_id.txt")));
			String line = reader.readLine();
			serveurId = Integer.parseInt(line.replace("serveur_id=", ""));
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				reader.close();
			} catch (IOException e) { 
				e.printStackTrace();
			}
		}

		serveur = ServicesAdministrateur.getServeurById(serveurId);
	}

	private static void stopElection(){
		serveur.setVotes(ServicesAdministrateur.getListVote());
		serveur.leader();
		serveur.envoyerResultatsAuChef();
	}

}