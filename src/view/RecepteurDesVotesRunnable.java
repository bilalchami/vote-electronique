package view;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import db.Services;
import services.ServicesAdministrateur;
import model.Candidat;
import model.Resultat;
import model.Serveur;
import model.SimpleVote;
import model.Vote;

public class RecepteurDesVotesRunnable implements Runnable {

	private ServerSocket serverSocket;
	private Socket socket;
	private List<Serveur> servers;

	public RecepteurDesVotesRunnable(ServerSocket serverSocket, Serveur serveurDepouillement) {
		this.serverSocket = serverSocket;
		servers = ServicesAdministrateur.getServeurList();
		servers.remove(serveurDepouillement);

	}

	@Override
	public void run() {
		try {
			System.out.println("****************************************************");
			System.out.println("\tServeur de d�pouillement �lu!");
			System.out.println("En attente la fin d'�l�tion sur les autres serveurs!");
			System.out.println("****************************************************");
			Iterator<Serveur> iterator = this.servers.iterator();
			while(iterator.hasNext()){
				socket = serverSocket.accept();

				Resultat resultatsVote = getResultats();

				Serveur s = Services.getServeurById(resultatsVote.getServeurId());

				List<Vote> newVotes = new ArrayList<Vote>();

				for(SimpleVote sv : resultatsVote.getVotes()){
					Vote vote = new Vote(Services.getElectionById(sv.getElectionId()), 
							Services.getElecteurById(sv.getElecteurId()),
							Services.getCandidatById(sv.getCandidatId()),
							sv.getDateVote());
					newVotes.add(vote);
				}

				Services.addVotes(newVotes);
				this.servers.remove(s);
			}

			for(Entry<String, Integer> entry : calculerResultats(Services.getListVote()).entrySet()){
				Candidat candidat = Services.getCandidatById(entry.getKey());
				System.out.println(candidat.getFullName() + ": " + entry.getValue());
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Resultat getResultats() {

		InputStream is;
		try {
			is = socket.getInputStream();
			ObjectInputStream ois = new ObjectInputStream(is);

			Resultat resultatsVote = (Resultat) ois.readObject();

			return resultatsVote;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	private HashMap<String, Integer> calculerResultats(List<Vote> listVotes){
		HashMap<String, Integer> votes = new HashMap<>();
		for(Vote vote : listVotes){
			if(votes.containsKey(vote.getCandidat().getId())){
				votes.put(vote.getCandidat().getId(), votes.get(vote.getCandidat().getId())+1);
			}else{
				votes.put(vote.getCandidat().getId(), 1);
			}
		}
		return votes;
	}

}
