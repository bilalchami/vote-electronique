package db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import model.Bureau;
import model.Candidat;
import model.Console;
import model.Electeur;
import model.Election;
import model.Serveur;
import model.Vote;

public class Services {
	static Connection connection;

	public static void main(String[] args) {
		System.out.println("Try to login on console 1 (Bureau 1) - Elie is on Bureau 1: " + login("Abi Hanna Daher","1","1"));
		System.out.println("Try to login on console 2 (Bureau 1) - Elie is on Bureau 1: " + login("Abi Hanna Daher","1","2"));
		System.out.println("Try to login on console 3 (Bureau 2) - Elie is on Bureau 1: " + login("Abi Hanna Daher","1","3"));
		for(Candidat candidat : getListCandidat()){
			System.out.println(candidat);
		}
		System.out.println("Elie a vote ? : " + getElecteurHasVotedInCurrentElection(login("Abi Hanna Daher","1","1")));
		System.out.println("Current Election : " + getElectionById(getCurrentElectionId()));
		System.out.println("Add Vote Electeur 1 vote To Candidat 1 : ");
		addVote("1","1");
		for(Vote vote : getListVote()){
			System.out.println(vote);
		}
		System.out.println(getServeurById(1));

	}

	public static Electeur login(String nom, String numero, String consoleId){
		Electeur electeur = null; 
		try { 
			if (connection == null){
				connection = ConnectionDB.connect();
			}
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery( "SELECT * FROM ELECTEUR WHERE ID='" + numero + "' and lower(nom) = '" + nom.toLowerCase() +"';" );
			while(rs.next()){  
				electeur = new Electeur(rs.getString("id"), rs.getString("prenom"), rs.getString("nom"), rs.getString("sexe"), rs.getDate("date_de_naissance"), getBureauById(rs.getString("bureau_id")));
				electeur.setVoted(getElecteurHasVotedInCurrentElection(electeur)); 
				if(electeur.getBureau().getId().equals(getConsoleById(consoleId).getBureau().getId())){ 
					return electeur;
				}else{
					return null;
				}
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return electeur;
	}

	public static Console getConsoleById(String id){
		Console console = null; 
		try { 
			if (connection == null){
				connection = ConnectionDB.connect();
			}
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM CONSOLE WHERE ID='" + id + "';" );
			while(rs.next()){  
				console = new Console(id, rs.getString("hote"), rs.getInt("port"), getBureauById(rs.getString("bureau_id"))); 
				return console;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return console;
	}

	public static Electeur getElecteurById(String id){
		Electeur electeur = null; 
		try { 
			if (connection == null){
				connection = ConnectionDB.connect();
			}
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM ELECTEUR WHERE ID='" + id + "';" );
			while(rs.next()){  
				electeur = new Electeur(rs.getString("id"), rs.getString("prenom"), rs.getString("nom"), rs.getString("sexe"), rs.getDate("date_de_naissance"), getBureauById(rs.getString("bureau_id")));
				electeur.setVoted(getElecteurHasVotedInCurrentElection(electeur));
				return electeur;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return electeur;
	}

	public static Bureau getBureauById(String id){
		Bureau bureau = null;
		try { 
			if (connection == null){
				connection = ConnectionDB.connect();
			}
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM BUREAU WHERE ID='" + id + "';" );
			while(rs.next()){  
				bureau = new Bureau(rs.getString("id"), rs.getString("location"), rs.getString("name"), getServeurById(rs.getInt("serveur_id")));
				return bureau;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		
		return bureau;
	}

	public static Serveur getServeurById(int id) { 
		Serveur serveur = null;
		try { 
			if (connection == null){
				connection = ConnectionDB.connect();
			}
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM SERVEUR WHERE ID='" + id + "';" );
			while(rs.next()){  
				serveur = new Serveur(rs.getInt("id"), rs.getString("hote"), rs.getInt("port"), rs.getInt("serveur_suivant_id"));
				return serveur;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}
		return serveur;
	}

	public static String getCurrentElectionId(){
		String electionId = ""; 
		try { 
			if (connection == null){
				connection = ConnectionDB.connect();
			}
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM ELECTION;" );
			while(rs.next()){  
				if(nowBetween(rs.getTimestamp("DEBUT_ELECTION"), rs.getTimestamp("FIN_ELECTION"))){
					return rs.getString("id");
				}
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return electionId;
	}

	public static List<Candidat> getListCandidat(){
		List<Candidat> list = new ArrayList<Candidat>(); 
		try { 
			if (connection == null){
				connection = ConnectionDB.connect();
			}
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM ELECTION_CANDIDAT WHERE ELECTION_ID='" + getCurrentElectionId() + "';" );
			while(rs.next()){  
				list.add(getCandidatById(rs.getString("candidat_id")));
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return list;
	}

	public static Candidat getCandidatById(String id) {		
		Candidat candidat = null; 
		try { 
			if (connection == null){
				connection = ConnectionDB.connect();
			}
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM CANDIDAT WHERE ID='" + id + "';" );
			while(rs.next()){  
				candidat = new Candidat(id, getElecteurById(rs.getString("electeur_id")));
				return candidat;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return candidat; 
	}

	public static Election getElectionById(String id){
		Election election = null; 
		try { 
			if (connection == null){
				connection = ConnectionDB.connect(); 
			}
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery( "SELECT * FROM ELECTION WHERE ID='" + id + "';" );
			while(rs.next()){  
				election = new Election(id, rs.getTimestamp("DEBUT_ELECTION"), rs.getTimestamp("FIN_ELECTION"), rs.getString("DESCRIPTION"), getServeurById(rs.getInt("SERVEUR_DEPOUILLEMENT_ID")));
				return election;	
			}			 
		} catch (SQLException e) { 
			e.printStackTrace();
		}		 
		return election;
	}

	//Check if electeur has voted in the election
	public static boolean getElecteurHasVotedInCurrentElection(Electeur electeur){ 
		try { 
			if (connection == null){
				connection = ConnectionDB.connect();
			}
			Statement st = connection.createStatement(); 
			ResultSet rs = st.executeQuery( "SELECT * FROM VOTE WHERE ELECTION_ID='" + getCurrentElectionId() + "' and electeur_id='"+ electeur.getId() +"';" );
			while(rs.next()){  
				return true;
			}			 
			return false;
		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return false;
	}

	//List of votes
	public static List<Vote> getListVote(){
		List<Vote> list = new ArrayList<Vote>(); 
		try { 
			if (connection == null){
				connection = ConnectionDB.connect();
			}
			Statement st = connection.createStatement();
			String electionId = getCurrentElectionId();
			ResultSet rs = st.executeQuery( "SELECT * FROM VOTE WHERE ELECTION_ID='" + electionId + "';" );
			while(rs.next()){  
				Vote vote = new Vote(getElectionById(electionId), getElecteurById(rs.getString("electeur_id")), getCandidatById(rs.getString("candidat_id")),rs.getTimestamp("date_vote"));
				list.add(vote);		
			}			 
		} catch (SQLException e) { 
			e.printStackTrace();
		}		 
		return list;
	}

	public static void addVotes(List<Vote> votes){
		for (Vote vote : votes){
			addVote(vote);
		}
	}

	public static void addVote(String electeurId, String candidatId){
		Vote vote = new Vote(getElectionById(getCurrentElectionId()), getElecteurById(electeurId), getCandidatById(candidatId), new Date());
		addVote(vote);
	}

	public static void addVote(Vote vote){
		try {
			if (connection == null){
				connection = ConnectionDB.connect();
			}
			Statement st = connection.createStatement();
			st.executeUpdate(vote.getInsertQuery());
		} catch (SQLException e) { 
			e.printStackTrace();
		}
	}

	//Utils methods
	private static boolean nowBetween(Date debut, Date fin) {
		Date now = new Date();
		return debut.compareTo(now) * now.compareTo(fin) > 0;
	}

}