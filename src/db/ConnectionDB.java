package db;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection; 
import java.sql.DriverManager;

public class ConnectionDB {

	private static String databaseName;
	private static String username;
	private static String password;
	private static String ip = "localhost";
	private static String port;

	public static Connection connect(){
		setDBInfo();
		try{
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://" + ip + ":" + port + "/" + databaseName;
			Connection connection = DriverManager.getConnection(url, username, password);
			return connection;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return null;	
	}

	private static void setDBInfo(){ 
		BufferedReader reader=null;

		try{
			reader = new BufferedReader(new FileReader(new File("db_info.txt")));
			String line = reader.readLine();
			databaseName = line.replace("databaseName=", "");
			line = reader.readLine();
			username =  line.replace("username=", "");
			line = reader.readLine();
			password =  line.replace("password=", "");
			line = reader.readLine();
			port = line.replace("port=", "");
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				reader.close();
			} catch (IOException e) { 
				e.printStackTrace();
			}
		}
	} 
}