package model;

import java.io.Serializable;

public class ElectionCandidat implements Serializable{

	private static final long serialVersionUID = -3395831608182827115L;

	//Attributes
	private Candidat candidat;
	private Election election;

	//Constructors
	public ElectionCandidat(Candidat candidat, Election election) {
		this.candidat = candidat;
		this.election = election;
	}

	//Getters and Setters
	public Candidat getCandidat() {
		return candidat;
	}

	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}

	public Election getElection() {
		return election;
	}

	public void setElection(Election election) {
		this.election = election;
	}

	//Methods
	@Override
	public String toString() {
		return "ElectionCandidat [candidat=" + candidat + ", election="
				+ election + "]";
	}
}