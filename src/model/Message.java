package model;

import java.io.Serializable;

public class Message implements Serializable {
	public static final int REQUETE = 0;
	public static final int CONFIRMATION = 1;

	private static final long serialVersionUID = 2338094409135163799L;

	private int type;
	private int expediteurId;
	private int destinataireId;
	private int leaderId;

	public Message(int type, int expediteurId, int destinataireId, int leaderId){
		this.type = type;
		this.expediteurId = expediteurId;
		this.destinataireId = destinataireId;
		this.leaderId = leaderId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getExpediteurId() {
		return expediteurId;
	}

	public void setExpediteurId(int expediteurId) {
		this.expediteurId = expediteurId;
	}

	public int getDestinataireId() {
		return destinataireId;
	}

	public void setDestinataireId(int destinataireId) {
		this.destinataireId = destinataireId;
	}


	public int getLeaderId() {
		return leaderId;
	}

	public void setLeaderId(int leaderId) {
		this.leaderId = leaderId;
	}

	@Override
	public String toString() {
		return "Message [type: " + type + ", expediteurId: " + expediteurId
				+ ", destinataireId: " + destinataireId + ", leaderId: "
				+ leaderId + "]";
	}

}
