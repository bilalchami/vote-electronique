package model;

import java.io.Serializable;
import java.util.Date;

public class SimpleVote implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1363666225914189020L;

	private String electionId;
	private String electeurId;
	private String candidatId; 
	private Date dateVote;

	public SimpleVote(Vote vote){
		this.electionId = vote.getElection().getId();
		this.electeurId = vote.getElecteur().getId();
		this.candidatId = vote.getCandidat().getId();
		this.dateVote = vote.getDateVote();
	}

	public String getElectionId() {
		return electionId;
	}

	public void setElectionId(String electionId) {
		this.electionId = electionId;
	}

	public String getElecteurId() {
		return electeurId;
	}

	public void setElecteurId(String electeurId) {
		this.electeurId = electeurId;
	}

	public String getCandidatId() {
		return candidatId;
	}

	public void setCandidatId(String candidatId) {
		this.candidatId = candidatId;
	}

	public Date getDateVote() {
		return dateVote;
	}

	public void setDateVote(Date dateVote) {
		this.dateVote = dateVote;
	}



}
