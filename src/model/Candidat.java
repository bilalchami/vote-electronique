package model;

import java.io.Serializable;

public class Candidat extends Electeur implements Serializable{

	private static final long serialVersionUID = 1708032237959416324L;

	//Attributes
	private String id; 

	//Constructors
	public Candidat(String id, Electeur electeur) {
		super(electeur);
		this.id = id;
	}

	//Getters and Setters
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	//Methods
	@Override
	public String toString() {
		return "Candidat [id=" + id + "] - " + super.toString();
	}

	public String getName(){
		return id + " : " + getFullName();
	}

}