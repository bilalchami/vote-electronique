package model;

import java.io.Serializable;
import java.util.Date;

public class Vote implements Serializable {

	private static final long serialVersionUID = -7161811693517829037L;

	//Attributes
	private Election election;
	private Electeur electeur;
	private Candidat candidat; 
	private Date dateVote;

	//Constructors
	public Vote(Election election, Electeur electeur, Candidat candidat) { 
		this.election = election;
		this.electeur = electeur;
		this.candidat = candidat;
		this.dateVote = new Date();
	}

	public Vote(Election election, Electeur electeur, Candidat candidat, Date dateVote) { 
		this.election = election;
		this.electeur = electeur;
		this.candidat = candidat;
		this.dateVote = dateVote;
	}

	//Getters and Setters
	public Election getElection() {
		return election;
	}

	public void setElection(Election election) {
		this.election = election;
	}

	public Electeur getElecteur() {
		return electeur;
	}

	public void setElecteur(Electeur electeur) {
		this.electeur = electeur;
	}

	public Candidat getCandidat() {
		return candidat;
	}

	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}

	public Date getDateVote() {
		return dateVote;
	}

	public void setDateVote(Date dateVote) {
		this.dateVote = dateVote;
	}

	//Methods
	@Override
	public String toString() {
		return "Vote [election=" + election + ", electeur=" + electeur
				+ ", candidat=" + candidat + "]";
	}

	public String getInsertQuery(){
		String query =  "INSERT INTO vote"
				+  "(election_id, electeur_id, candidat_id, date_vote) VALUES "
				+  "('" + this.election.getId() + "','" + this.electeur.getId() + "','" + this.candidat.getId() + "','" + this.getDateVote() +"');" ;
		return query;
	}


}
