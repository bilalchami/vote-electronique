package model;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.util.ArrayList;
import java.util.List;

import db.Services;
import services.ServicesAdministrateur;
import services.ServicesVote;
import view.ProblemeElectionRunnable;
import view.ConsoleRunnable;
import view.RecepteurDesVotesRunnable;

public class Serveur implements Serializable {

	public static final int REPOS = 0;
	public static final int EN_COURS = 1;
	public static final int TERMINE = 2;

	private static final long serialVersionUID = -4487231262590177077L;

	//Attributes
	private int id; 
	private String hote; 
	private int port;
	private int serveurSuivantId;
	private Serveur chef;
	private List<Vote> votes;

	//Constructor
	public Serveur(int id, String hote, int port, int serveurSuivantId) {
		this.id = id;
		this.hote = hote;
		this.port = port;
		this.serveurSuivantId = serveurSuivantId;
		this.votes = new ArrayList<Vote>();
	}


	//Getters and Setters
	public String getHote() {
		return hote;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setHote(String hote) {
		this.hote = hote;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getServeurSuivantId() {
		return serveurSuivantId;
	}

	public void setServeurSuivantId(int serveurSuivantId) {
		this.serveurSuivantId = serveurSuivantId;
	}

	public Serveur getChef() {
		return chef;
	}

	public void setChef(Serveur chef) {
		this.chef = chef;
	}

	public List<Vote> getVotes() {
		return votes;
	}


	public void setVotes(List<Vote> votes) {
		this.votes = votes;
	}


	//Methods
	@Override
	public String toString() {
		return "Serveur [id=" + id + ", hote=" + hote + ", port=" + port + ", Serveur suivant ID : " + serveurSuivantId +  "]";
	}

	public void startServer(){
		try{
			System.setProperty("java.rmi.server.hostname",this.hote);
			Naming.rebind("rmi://" + hote + ":" + 1099 + "/Serveur" + this.id, new ServicesVote());
			System.out.println("Serveur pret!!");
			serverSocket = new ServerSocket(port);
			problemeElectionRunnable = new ProblemeElectionRunnable(this, serverSocket);
			Thread threadConnecteur = new Thread(problemeElectionRunnable);
			threadConnecteur.start();

			serverSocketConsoleListener = new ServerSocket(port+1);

			consoleListener = new ConsoleRunnable(serverSocketConsoleListener);

			Thread threadListener = new Thread(consoleListener);
			threadListener.start();
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	private int etat;
	private ServerSocket serverSocket;
	private Socket socket;
	private ProblemeElectionRunnable problemeElectionRunnable;
	private ConsoleRunnable consoleListener;

	private ServerSocket serverSocketConsoleListener;
	//	private int portConsole = port+1;

	public void leader(){
		if (etat == Serveur.REPOS) {
			etat = Serveur.EN_COURS;
			chef = this;
			Message requete = new Message(Message.REQUETE, this.id, this.serveurSuivantId, this.id);
			envoyerASuivant(requete);
		}
	}

	public void surReceptionDeRequete(Message message){
		int k = message.getLeaderId();
		int chefId = chef==null?-1:chef.getId();

		if(etat==Serveur.REPOS || (k<chefId)){
			this.etat = Serveur.EN_COURS;
			this.chef = Services.getServeurById(k);
			Message requete = new Message(Message.REQUETE, this.id, this.serveurSuivantId, k);
			envoyerASuivant(requete);
		}else if(this.id==k){
			this.etat = Serveur.TERMINE;
			Message confirmation = new Message(Message.CONFIRMATION, this.id, this.serveurSuivantId, k);
			envoyerASuivant(confirmation);
		}
	}

	public void surReceptionDeConfirmation(Message message){
		int k = message.getLeaderId();
		if(this.id!=k){
			Message confirmation = new Message(Message.CONFIRMATION, this.id, this.serveurSuivantId, k);
			envoyerASuivant(confirmation);
			this.etat = Serveur.TERMINE;
		}else{
			startRecepteurDesVotes();
		}
		ServicesAdministrateur.updateServeurDeDepouillement(this.chef);
	}

	public void envoyerResultatsAuChef() {
		if(this.chef.getId()!=this.id){
			Socket socketResultats = null;
			try {
				List<SimpleVote> listSimpleVote = new ArrayList<SimpleVote>();
				for (Vote vote : votes){
					SimpleVote sVote = new SimpleVote(vote);
					listSimpleVote.add(sVote);
				}
				Resultat resultatsVote = new Resultat(this.getId(), listSimpleVote);

				socketResultats = new Socket(chef.getHote(),chef.getPort()+2);
				ObjectOutputStream out = new ObjectOutputStream(socketResultats.getOutputStream());
				out.writeObject(resultatsVote);
				out.close();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void startRecepteurDesVotes(){
		try {
			ServerSocket recepteurDesVotesServerSocket = new ServerSocket(port+2);
			RecepteurDesVotesRunnable recepteurDesVotes = new RecepteurDesVotesRunnable(recepteurDesVotesServerSocket, chef);
			Thread threadRecepteurVotes = new Thread(recepteurDesVotes);
			threadRecepteurVotes.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void envoyerASuivant(Message message){
		Serveur serveurSuivant = Services.getServeurById(serveurSuivantId);
		try {
			socket = new Socket(serveurSuivant.getHote(),serveurSuivant.getPort());
			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			out.writeObject(message);
			out.close();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Serveur other = (Serveur) obj;
		if (id != other.id)
			return false;
		return true;
	}

}