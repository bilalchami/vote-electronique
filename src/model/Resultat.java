package model;

import java.io.Serializable; 
import java.util.List;

public class Resultat implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8509055910762862146L;
	//Attribut
	private int serveurId; 
	private List<SimpleVote> votes;

	public Resultat(int serveurId, List<SimpleVote> votes) {
		super();
		this.serveurId = serveurId;
		this.votes = votes;
	}

	//Getters and Setters
	public int getServeurId() {
		return serveurId;
	}
	public void setServeurId(int serveurId) {
		this.serveurId = serveurId;
	}



	public List<SimpleVote> getVotes() {
		return votes;
	}

	public void setVotes(List<SimpleVote> votes) {
		this.votes = votes;
	}

	//Methods
	public void afficherResultat(){
		for(SimpleVote v : votes){
			System.out.println(v);
		}
	}




}
