package model;

import java.io.Serializable;
import java.rmi.Naming;
import java.util.UUID;

import services.ServicesVoteInterface;

public class Console implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1746893293944994077L;

	//Attributes
	private String id;
	private String hote;
	private int port;
	private Bureau bureau;
	private ServicesVoteInterface services;

	//Constructor
	public Console(String id, String hote, int port, Bureau bureau){
		this.id = id;
		this.hote = hote;
		this.port = port;
		this.bureau = bureau;
	}
	public Console(String id, String hote, int port, Bureau bureau, boolean start) {
		this.id = id;
		this.hote = hote;
		this.port = port;
		this.bureau = bureau;
	}

	public Console(String hote, int port, Bureau bureau) {
		this.id = UUID.randomUUID().toString();
		this.hote = hote;
		this.port = port;
		this.bureau = bureau;
	}

	//Getters and Setters
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHote() {
		return hote;
	}

	public void setHote(String hote) {
		this.hote = hote;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public Bureau getBureau() {
		return bureau;
	}

	public void setBureau(Bureau bureau) {
		this.bureau = bureau;
	}

	public ServicesVoteInterface getServices() {
		return services;
	}

	public void setServices(ServicesVoteInterface services) {
		this.services = services;
	}

	//Methods
	@Override
	public String toString() {
		return "Console [id=" + id + ", hote=" + hote + ", port=" + port + "]";
	}

	public void startClient(){
		try{
			this.services = 
					(ServicesVoteInterface) 
					Naming.lookup("rmi://" + this.bureau.getServeur().getHote() + ":" + 1099 + "/Serveur" + this.bureau.getServeur().getId());
		}catch (Exception e){
			e.printStackTrace();
		}
	}

}