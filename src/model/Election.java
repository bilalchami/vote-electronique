package model;

import java.io.Serializable;
import java.util.Date;

import db.Services;

public class Election implements Serializable{

	private static final long serialVersionUID = -5370205270792254796L;

	//Attributes
	private String id; 
	private Date debutElection;
	private Date finElection;
	private String description;
	private Serveur serveurDeDepouillement;

	//Constructors
	public Election(String id, Date debutElection, Date finElection, String description) {
		this.id = id;
		this.debutElection = debutElection;
		this.finElection = finElection;
		this.description = description;

		//to set Serveur de depouillement par Algorithme d'election
	} 
	public Election(String id, Date debutElection, Date finElection, String description, Serveur serveurDeDepouillement) {
		this.id = id;
		this.debutElection = debutElection;
		this.finElection = finElection;
		this.description = description; 
		this.serveurDeDepouillement = serveurDeDepouillement;
	} 

	//Getters and Setters
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDebutElection() {
		return debutElection;
	}

	public void setDebutElection(Date debutElection) {
		this.debutElection = debutElection;
	}

	public Date getFinElection() {
		return finElection;
	}

	public void setFinElection(Date finElection) {
		this.finElection = finElection;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Serveur getServeurDeDepouillement() {
		return serveurDeDepouillement;
	}

	public void setServeurDeDepouillement(Serveur serveurDeDepouillement) {
		this.serveurDeDepouillement = serveurDeDepouillement;
	}

	//Methods
	@Override
	public String toString() {
		return "Election [id=" + id + ", debutElection=" + debutElection
				+ ", finElection=" + finElection + "]";
	}

	public String getUpdateServeurDepouillementQuery(int serveurDeDepouillementId){
		String query = "UPDATE election SET " 
				+ "serveur_depouillement_id ='" + this.serveurDeDepouillement.getId() + "'"
				+ "WHERE id='" + this.id +"';" ;
		return query;
	}

	public String getUpdateQuery(Date finElection, int serveurDeDepouillementId){
		this.finElection = finElection;
		this.serveurDeDepouillement = Services.getServeurById(serveurDeDepouillementId);
		String query = "UPDATE election SET " 
				+ "debutElection ='" + this.debutElection + "', "
				+ "finElection ='" + this.finElection + "', "
				+ "description ='" + this.description + "', "
				+ "serveur_depouillement_id ='" + this.serveurDeDepouillement.getId() + "'"
				+ "WHERE id='" + this.id +"';" ;
		return query;
	}

}