package model;

import java.io.Serializable;
import java.util.Date;

public class Electeur implements Serializable{

	private static final long serialVersionUID = -3690470190031436683L;

	//Attributes
	private String id; 
	private String prenom; 
	private String nom; 
	private String sexe; 
	private Date dateDeNaissance; 
	private boolean voted; 
	private Bureau bureau;

	//Constructors
	public Electeur(String id, String prenom, String nom, String sexe,
			Date dateDeNaissance, Bureau bureau) {
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.sexe = sexe;
		this.dateDeNaissance = dateDeNaissance;
		//this.listVote = listVote;
		this.bureau = bureau;
	}

	public Electeur(Electeur electeur) {
		this.id = electeur.getId();
		this.prenom = electeur.getPrenom();
		this.nom = electeur.getNom();
		this.sexe = electeur.getSexe();
		this.dateDeNaissance = electeur.getDateDeNaissance();
		//this.listVote = electeur.getListVote();
		this.bureau = electeur.getBureau();
	}

	//Getters and Setters
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public boolean hasVoted() {
		return voted;
	}

	public void setVoted(boolean voted) {
		this.voted = voted;
	}

	//	public List<Vote> getListVote() {
	//		return listVote;
	//	}
	//
	//	public void setListVote(List<Vote> listVote) {
	//		this.listVote = listVote;
	//	}
	public Bureau getBureau() {
		return bureau;
	}

	public void setBureau(Bureau bureau) {
		this.bureau = bureau;
	}

	//Methods
	@Override
	public String toString() {
		return "Electeur [id=" + id + ", prenom=" + prenom + ", nom=" + nom
				+ ", sexe=" + sexe + ", dateDeNaissance=" + dateDeNaissance
				+ "]";
	}

	public String getFullName(){
		return prenom + " " + nom;
	}

}