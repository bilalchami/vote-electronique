package model;

import java.io.Serializable;
import java.util.UUID;

public class Bureau implements Serializable{

	private static final long serialVersionUID = 2541773044834780801L;

	//Attributes
	private String id;
	private String location;
	private String nom;
	private Serveur serveur; 

	//Constructor
	public Bureau(String id, String location, String nom, Serveur serveur){
		this.id = id;
		this.location = location;
		this.nom = nom;
		this.serveur = serveur;
	}

	public Bureau(String location, String nom, Serveur serveur){
		this.id = UUID.randomUUID().toString();
		this.location = location;
		this.nom = nom;
		this.serveur = serveur; 
	}

	//Getters and Setters
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Serveur getServeur() {
		return serveur;
	}

	public void setServeur(Serveur serveur) {
		this.serveur = serveur;
	}

	//Methods
	@Override
	public String toString() {
		return "Bureau [id=" + id + ", location=" + location + ", nom=" + nom + "]";
	}

}