package services;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import db.ConnectionDB;
import model.Bureau;
import model.Candidat;
import model.Console;
import model.Electeur;
import model.Election;
import model.Serveur;
import model.Vote;

public class ServicesVote extends java.rmi.server.UnicastRemoteObject implements ServicesVoteInterface{

	private static final long serialVersionUID = 5524423875580639658L;

	private Connection connection;

	public ServicesVote() throws RemoteException {
		super();
		connection = ConnectionDB.connect();
	}

	@Override
	public Electeur login(String nom, String numero, String consoleId) throws RemoteException {
		Electeur electeur = null; 
		try {
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery( "SELECT * FROM ELECTEUR WHERE ID='" + numero + "' and lower(nom) = '" + nom.toLowerCase() +"';" );
			while(rs.next()){  
				electeur = new Electeur(rs.getString("id"), rs.getString("prenom"), rs.getString("nom"), rs.getString("sexe"), rs.getDate("date_de_naissance"), getBureauById(rs.getString("bureau_id")));
				if(electeur.getBureau().getId().equals(getConsoleById(consoleId).getBureau().getId())){
					electeur.setVoted(getElecteurHasVotedInCurrentElection(electeur));
					return electeur;
				}else{
					return null;
				}
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return electeur;
	}

	@Override
	public Console getConsoleById(String id) throws RemoteException{
		Console console = null; 
		try {
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery( "SELECT * FROM CONSOLE WHERE ID='" + id + "';" );
			while(rs.next()){  
				console = new Console(id, rs.getString("hote"), rs.getInt("port"), getBureauById(rs.getString("bureau_id")));

				return console;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return console;
	}

	@Override
	public Electeur getElecteurById(String id) throws RemoteException {
		Electeur electeur = null; 
		try {
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM ELECTEUR WHERE ID='" + id + "';" );
			while(rs.next()){  
				electeur = new Electeur(rs.getString("id"), rs.getString("prenom"), rs.getString("nom"), rs.getString("sexe"), rs.getDate("date_de_naissance"), getBureauById(rs.getString("bureau_id")));
				electeur.setVoted(getElecteurHasVotedInCurrentElection(electeur));
				return electeur;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return electeur;
	}

	@Override
	public Bureau getBureauById(String id) throws RemoteException {
		Bureau bureau = null;
		try {
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM BUREAU WHERE ID='" + id + "';" );
			while(rs.next()){  
				bureau = new Bureau(rs.getString("id"), rs.getString("location"), rs.getString("name"), getServeurById(rs.getInt("serveur_id")));
				return bureau;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		
		return bureau;
	}

	@Override
	public Serveur getServeurById(int id) throws RemoteException { 
		Serveur serveur = null;
		try {
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM SERVEUR WHERE ID='" + id + "';" );
			while(rs.next()){  
				serveur = new Serveur(rs.getInt("id"), rs.getString("hote"), rs.getInt("port"), rs.getInt("serveur_suivant_id"));
				return serveur;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}
		return serveur;
	}

	@Override
	public String getCurrentElectionId() throws RemoteException {
		String electionId = ""; 
		try {
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM ELECTION;" );
			while(rs.next()){  
				if(nowBetween(rs.getTimestamp("DEBUT_ELECTION"), rs.getTimestamp("FIN_ELECTION"))){
					return rs.getString("id");
				}
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return electionId;
	}

	@Override
	public List<Candidat> getListCandidat() throws RemoteException {
		List<Candidat> list = new ArrayList<Candidat>(); 
		try {
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM ELECTION_CANDIDAT WHERE ELECTION_ID='" + getCurrentElectionId() + "';" );
			while(rs.next()){  
				list.add(getCandidatById(rs.getString("candidat_id")));
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return list;
	}

	@Override
	public Candidat getCandidatById(String id) throws RemoteException {		
		Candidat candidat = null; 
		try {
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM CANDIDAT WHERE ID='" + id + "';" );
			while(rs.next()){  
				candidat = new Candidat(id, getElecteurById(rs.getString("electeur_id")));
				return candidat;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return candidat; 
	}

	@Override
	public Election getElectionById(String id) throws RemoteException {
		Election election = null; 
		try {
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery( "SELECT * FROM ELECTION WHERE ID='" + id + "';" );
			while(rs.next()){  
				election = new Election(id, rs.getTimestamp("DEBUT_ELECTION"), rs.getTimestamp("FIN_ELECTION"), rs.getString("DESCRIPTION"), getServeurById(rs.getInt("SERVEUR_DEPOUILLEMENT_ID")));
				return election;	
			}			 
		} catch (SQLException e) { 
			e.printStackTrace();
		}		 
		return election;
	}

	public boolean getElecteurHasVotedInCurrentElection(Electeur electeur) throws RemoteException { 
		try {
			Statement st = connection.createStatement(); 
			ResultSet rs = st.executeQuery( "SELECT * FROM VOTE WHERE ELECTION_ID='" + getCurrentElectionId() + "' and electeur_id='"+ electeur.getId() +"';" );
			while(rs.next()){  
				return true;
			}			 
			return false;
		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return false;
	}

	@Override
	public List<Vote> getListVote() throws RemoteException {
		List<Vote> list = new ArrayList<Vote>(); 
		try {
			Statement st = connection.createStatement();
			String electionId = getCurrentElectionId();
			ResultSet rs = st.executeQuery( "SELECT * FROM VOTE WHERE ELECTION_ID='" + electionId + "';" );
			while(rs.next()){  
				Vote vote = new Vote(getElectionById(electionId), getElecteurById(rs.getString("electeur_id")), getCandidatById(rs.getString("candidat_id")),rs.getTimestamp("date_vote"));
				list.add(vote);		
			}			 
		} catch (SQLException e) { 
			e.printStackTrace();
		}		 
		return list;
	}

	@Override
	public void addVotes(List<Vote> votes) throws RemoteException {
		for (Vote vote : votes){
			addVote(vote);
		}
	}

	@Override
	public void addVote(String electeurId, String candidatId) throws RemoteException {
		Vote vote = new Vote(getElectionById(getCurrentElectionId()), getElecteurById(electeurId), getCandidatById(candidatId), new Date());
		addVote(vote);
	}

	@Override
	public void addVote(Vote vote) throws RemoteException {
		try {
			Statement st = connection.createStatement();
			st.executeUpdate(vote.getInsertQuery());
		} catch (SQLException e) { 
			e.printStackTrace();
		}
	}

	@Override
	public boolean nowBetween(Date debut, Date fin) throws RemoteException {
		Date now = new Date();
		return debut.compareTo(now) * now.compareTo(fin) > 0;
	}

}