package services;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import model.Bureau;
import model.Candidat;
import model.Console;
import model.Electeur;
import model.Election;
import model.Serveur;
import model.Vote;

public interface ServicesVoteInterface extends Remote{

	Electeur login(String nom, String numero, String consoleId) throws RemoteException;

	Console getConsoleById(String id) throws java.rmi.RemoteException;

	Electeur getElecteurById(String id) throws RemoteException;

	Bureau getBureauById(String id) throws RemoteException;

	Serveur getServeurById(int id) throws RemoteException;

	String getCurrentElectionId() throws RemoteException;

	List<Candidat> getListCandidat() throws RemoteException;

	Candidat getCandidatById(String id) throws RemoteException;

	Election getElectionById(String id) throws RemoteException;

	boolean getElecteurHasVotedInCurrentElection(Electeur electeur)
			throws RemoteException;

	List<Vote> getListVote() throws RemoteException;

	void addVotes(List<Vote> votes) throws RemoteException;

	void addVote(String electeurId, String candidatId) throws RemoteException;

	void addVote(Vote vote) throws RemoteException;

	boolean nowBetween(Date debut, Date fin) throws RemoteException;

}
