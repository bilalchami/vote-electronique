package services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import db.ConnectionDB;
import model.Bureau;
import model.Candidat;
import model.Electeur;
import model.Election;
import model.Serveur;
import model.Vote;

public class ServicesAdministrateur {

	static Connection connection = null;

	public static void updateServeurDeDepouillement(Serveur serveur){
		if(connection == null){
			connection = ConnectionDB.connect();
		}
		try {
			Statement st = connection.createStatement();
			String updateQuery = "UPDATE election SET " 
					+ "serveur_depouillement_id='"   + serveur.getId() + "' "
					+ "WHERE id='" + getCurrentElectionId() +"';" ; 
			st.executeUpdate(updateQuery);
		} catch (SQLException e) { 
			e.printStackTrace();
		}
	}

	public static List<Serveur> getServeurList(){
		List<Serveur> serveurs = new ArrayList<Serveur>();
		Connection connection = ConnectionDB.connect();
		try {
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM SERVEUR");
			while(rs.next()){  
				Serveur serveur = new Serveur(rs.getInt("id"), rs.getString("hote"), rs.getInt("port"), rs.getInt("serveur_suivant_id"));
				serveurs.add(serveur);
			}
		} catch (SQLException e) { 
			e.printStackTrace();
		}
		return serveurs;
	}

	public static List<Vote> getListVote(){
		List<Vote> list = new ArrayList<Vote>();
		Connection connection = ConnectionDB.connect();
		try {
			Statement st = connection.createStatement();
			String electionId = getCurrentElectionId();
			ResultSet rs = st.executeQuery( "SELECT * FROM VOTE WHERE ELECTION_ID='" + electionId + "';" );
			while(rs.next()){  
				Vote vote = new Vote(getElectionById(electionId), getElecteurById(rs.getString("electeur_id")), getCandidatById(rs.getString("candidat_id")),rs.getTimestamp("date_vote"));
				list.add(vote);		
			}			 
		} catch (SQLException e) { 
			e.printStackTrace();
		}		 
		return list;
	}

	private static Bureau getBureauById(String id) {
		Bureau bureau = null;
		if(connection == null){
			connection = ConnectionDB.connect();
		}
		try {
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM BUREAU WHERE ID='" + id + "';" );
			while(rs.next()){  
				bureau = new Bureau(rs.getString("id"), rs.getString("location"), rs.getString("name"), getServeurById(rs.getInt("serveur_id")));
				return bureau;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		
		return bureau;
	}

	private static Election getElectionById(String id) {
		Election election = null; 
		if(connection == null){
			connection = ConnectionDB.connect();
		}
		try {
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery( "SELECT * FROM ELECTION WHERE ID='" + id + "';" );
			while(rs.next()){  
				election = new Election(id, rs.getTimestamp("DEBUT_ELECTION"), rs.getTimestamp("FIN_ELECTION"), rs.getString("DESCRIPTION"), getServeurById(rs.getInt("SERVEUR_DEPOUILLEMENT_ID")));
				return election;	
			}			 
		} catch (SQLException e) { 
			e.printStackTrace();
		}		 
		return election;
	}

	private static Electeur getElecteurById(String id) {
		Electeur electeur = null; 		
		if(connection == null){
			connection = ConnectionDB.connect();
		}
		try {
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM ELECTEUR WHERE ID='" + id + "';" );
			while(rs.next()){  
				electeur = new Electeur(rs.getString("id"), rs.getString("prenom"), rs.getString("nom"), rs.getString("sexe"), rs.getDate("date_de_naissance"), getBureauById(rs.getString("bureau_id")));
				return electeur;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return electeur;
	}

	public static Serveur getServeurById(int id){ 
		Serveur serveur = null;
		if(connection == null){
			connection = ConnectionDB.connect();
		}
		try {
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM SERVEUR WHERE ID='" + id + "';" );
			while(rs.next()){  
				serveur = new Serveur(rs.getInt("id"), rs.getString("hote"), rs.getInt("port"), rs.getInt("serveur_suivant_id"));
				return serveur;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}
		return serveur;
	}

	private static Candidat getCandidatById(String id){		
		Candidat candidat = null; 
		if(connection == null){
			connection = ConnectionDB.connect();
		}
		try {
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM CANDIDAT WHERE ID='" + id + "';" );
			while(rs.next()){  
				candidat = new Candidat(id, getElecteurById(rs.getString("electeur_id")));
				return candidat;
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return candidat; 
	}

	private static String getCurrentElectionId(){
		String electionId = ""; 
		try {
			if(connection == null){
				connection = ConnectionDB.connect();
			}
			Statement st = connection.createStatement();

			ResultSet rs = st.executeQuery( "SELECT * FROM ELECTION;" );
			while(rs.next()){  
				if(nowBetween(rs.getTimestamp("DEBUT_ELECTION"), rs.getTimestamp("FIN_ELECTION"))){
					return rs.getString("id");
				}
			}			 

		} catch (SQLException e) { 
			e.printStackTrace();
		}		 

		return electionId;
	}

	private static boolean nowBetween(Date debut, Date fin){
		Date now = new Date();
		return debut.compareTo(now) * now.compareTo(fin) > 0;
	}

}